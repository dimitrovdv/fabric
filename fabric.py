import time
from fabric.api import run, put, env, sudo, task
import getpass
env.hosts = ['192.168.211.158']
env.user = 'sysadmin'
local_path = './files'
remote_file_path = '/tmp'
env.sudo_password = getpass.getpass(prompt='Введите пароль sudo:')

def sec_to_hours(seconds):
    a=str(seconds//3600)
    b=str((seconds%3600)//60)
    c=str((seconds%3600)%60)
    d=["{} hours {} mins {} seconds".format(a, b, c)]
    return d

@task
def install_kesl_full():
     # Засекаем начальное время
     tic = time.perf_counter()
     #Созадение директории tmp на удаленном хосте, если нет
     run('mkdir -p /tmp')
     #Копирование файла ответов дистрибутива
     put(local_path, remote_file_path)
     print('Копирование выполнено')
     ###################################################
     # Изменение разрешений и владельца
     sudo('chown root:root /var')
     sudo('chmod g-w,o-w /var')
     print('Изменение владельца и разрешение выполнено')
     # Установка пакетов
     sudo('dpkg -i /tmp/klnagent64_14.0.0-4490_amd64.deb')
     time.sleep(15)
     sudo('dpkg -i /tmp/kesl_10.1.1-6421_amd64.deb')
     time.sleep(5)
     # Запуск файлов ответа
     sudo('env KLAUTOANSWERS=/tmp/answers.txt /opt/kaspersky/klnagent64/lib/bin/setup/postinstall.pl')
     time.sleep(15)
     sudo('/opt/kaspersky/kesl/bin/kesl-setup.pl --autoinstall=/tmp/kesl.ini')
     # Засекаем конечное время
     toc = time.perf_counter()
     print(f"Вычисление заняло: {toc - tic:0.4f} секунд")
     print(sec_to_hours(toc - tic))


#requarments
'''bcrypt==4.0.1
cffi==1.15.1
cryptography==40.0.1
Fabric3==1.14.post1
invoke==2.0.0
paramiko==2.12.0
pycparser==2.21
PyNaCl==1.5.0
six==1.16.0
'''

#envirment
#python -m venv env
# .\env\Scripts\activate.bat
#python --version
#Python 3.8.0
#pip install -r requirements.txt
#fab --user=sysadmin --password=ghjgfcnm install_kesl_full
#ssh-copy-id -i ~/mykey.pem.pub root@192.168.216.128
#env.key_filename = '~/.ssh/mykey.pem'
